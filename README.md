gm2html is a set of tools for producing alternative views of map data from Open Green Map. It produces html that can be included on a webpage.
The map_export.csv file that can be exported for every map on Open Green Map is used as input, the tools parses it and creates html output with a complete list of sites, latest added sites, latest changed sites etc.

See http://www.opengreenmap.org for more information on Open Green Map.

For a demo of gm2hml see:
http://ekoblekinge.se/grona-platser (page displays output from gm2html.py)
http://ekoblekinge.se (sidebar widget displays output from gm2html-latest-brief.py)
http://ekoblekinge.se/2648/nya-platser-pa-blekinge-greenmap-5 (post displays output from gm2html-latest-complete.py)