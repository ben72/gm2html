#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    gm2html-latest-brief is a program for parsing map data exported from Open Greenmap
#    and creating html output for the latest added and latest changed sites.
#    Copyright 2010 Ben Andersen
#	 http://ekoblekinge.se
#
#    gm2html-latest-brief is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
import datetime
from dateutil import parser
from operator import itemgetter
import sys

MAX_NEW_SITES = 10	# Max number of new sites to display
MAX_LATEST_SITES = 10	# Max number of latest changed sites to display
MAP_FILE = "/home/ben/Skrivbord/map_export.csv"	# full path to map_export.csv file

arguments = sys.argv
if (len(sys.argv) != 2):
	print "Usage: " +  sys.argv[0] + " <Name of Green map>"
	sys.exit()

# print header with current date and time
now = datetime.datetime.now()
now_string = now.strftime("%Y-%m-%d kl %H:%M")
##print "<a href=\"http://ekoblekinge.se/greenmap\"><img border=\"0\" src=\"http://ekoblekinge.se/wp-content/uploads/opengreenmapblekinge.png\"></a>"
##print "Följande information hämtades från <a href=\"http://ekoblekinge.se/greenmap\">Blekinge Greenmap</a> " + now_string + "."
print "<b>Senast tillagda platser:</b>"
print "<br/>"

# read map file
fh_map = open(MAP_FILE, "r")
input_map = csv.DictReader(fh_map, delimiter=',')

# copy map file to list
total_site_count = 0
map_list = []
for line_map in input_map:
	if(line_map["Public Site"] == "Yes"):
		total_site_count = total_site_count+1
	# make dates useful for sorting
	dateTime = parser.parse(line_map["Authored On"])
	line_map["Authored On"] = str(dateTime)
	dateTime = parser.parse(line_map["Last Edited"])
	line_map["Last Edited"] = str(dateTime)
	map_list.append(line_map)


# sort list by Authored On date
sorted_list = sorted(map_list, key=itemgetter("Authored On"), reverse=True)

greenmap_name_for_url = "-" + sys.argv[1].lower()
if(greenmap_name_for_url == "-blekinge"):
	# blank for Blekinge greenmap
	greenmap_name_for_url = ""

print "<ul>"
# loop map file
site_count = 0
new_sites_list = []
for line_map in sorted_list:
	new_sites_list.append(line_map)

	# print each entry matching current Primary term
	site_count = site_count+1

#	dateTime = parser.parse(line_map["Authored On"])
#	print dateTime
	name_of_site = line_map["Name of Site"].replace(" ", "%20").replace("&", "&amp;")	# replace space with %20 for html
	print "<li><a href=\"http://ekoblekinge.se/grona-platser" + greenmap_name_for_url + "#" + name_of_site + "\">" + line_map["Name of Site"] + "</a></li>"

	if site_count >= MAX_NEW_SITES:
		break
print "</ul>"

# sort list by Last edited date
sorted_list = sorted(map_list, key=itemgetter("Last Edited"), reverse=True)

print "<b>Senast ändrade platser:</b>"
print "<br/>"

print "<ul>"
site_count = 0
# loop map file
for line_map in sorted_list:
	# print each entry matching current Primary term

	site_in_list = False
	for new_list_site in new_sites_list:
		if new_list_site["Site ID"] == line_map["Site ID"]:
			site_in_list = True
	if not site_in_list:
		site_count = site_count+1
		name_of_site = line_map["Name of Site"].replace(" ", "%20").replace("&", "&amp;")	# replace space with %20 for html
		print "<li><a href=\"http://ekoblekinge.se/grona-platser" + greenmap_name_for_url + "#" + name_of_site + "\">" + line_map["Name of Site"] + "</a></li>"
##		dateTime = parser.parse(line_map["Authored On"])
##		print dateTime

	if site_count > MAX_LATEST_SITES:
		break
print "</ul>"

#print "<a href=\"http://ekoblekinge.se/greenmap\"><img border=\"0\" src=\"http://ekoblekinge.se/wp-content/uploads/opengreenmapblekinge.png\"></a>"
print "<style type=\"text/css\">.gm-view-all a:link,.gm-view-all a:visited {color: #888;}.gm-view-all a:active,.gm-view-all a:hover {color: #888;}</style>"
print "<br><span class=\"gm-view-all\"><a class=\"gm-view-all\" href=\"http://ekoblekinge.se/grona-platser\">Visa alla " + str(total_site_count) + " gröna platser</a></span>"

