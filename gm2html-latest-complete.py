#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    gm2html-latest is a program for parsing map data exported from Open Greenmap
#    and creating html output for the latest added and latest changed sites.
#    Copyright 2011 Ben Andersen
#	 http://ekoblekinge.se
#
#    gm2html-latest is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
import datetime
import sys
from dateutil import parser
from operator import itemgetter

MAX_NEW_SITES = 15	# Max number of new sites to display
MAX_LATEST_SITES = 10	# Max number of latest changed sites to display
MAP_FILE = "/home/ben/Skrivbord/map_export.csv"	# full path to map_export.csv file
CONFIG_FILE = "gm2html.conf"	# full path to config file

arguments = sys.argv
if (len(sys.argv) != 4):
	print "Usage: " +  sys.argv[0] + " <Name of Green map> <URL to Green map> <URL to adding site to this Green map>"
	sys.exit()

# get all icons for a specific site as html
def getIcons(icon_input):
    fh_conf = open(CONFIG_FILE, "r")
    input_conf = csv.DictReader(fh_conf, delimiter=',')
    icon_array = icon_input.split(',')

    icons_string = ""
    for line_term in input_conf:
        for icon in icon_array:
            if(line_term["english"] == icon.strip()):
                icons_string = icons_string + " " + "<img border=\"0\" src=\"" + line_term["Primary Term"] + "\" title=\"" + line_term["swedish"] + "\">"
    return icons_string


# print some css for table rows
print "<style type=\"text/css\">tr.d1 td {background-color: #f4fae0;}</style>"

# print header with current date and time
now = datetime.datetime.now()
now_string = now.strftime("%Y-%m-%d kl %H:%M")
##print "<a href=\"http://ekoblekinge.se/greenmap\"><img border=\"0\" src=\"http://ekoblekinge.se/wp-content/uploads/opengreenmapblekinge.png\"></a>"
##print "Följande information hämtades från <a href=\"http://ekoblekinge.se/greenmap\">Blekinge Greenmap</a> " + now_string + "."
print "Följande information hämtades från <a href=\"" + sys.argv[2] + "\">" + sys.argv[1] + " Greenmap</a> " + now_string + "."
print "Det är enkelt att föreslå nya hållbara platser, <a href=\"" + sys.argv[3] + "\" target=_blank>dela dina gröna smultronställen här</a>."
print "<br/>"

# read map file
fh_map = open(MAP_FILE, "r")
input_map = csv.DictReader(fh_map, delimiter=',')

# copy map file to list
map_list = []
for line_map in input_map:
	# make dates useful for sorting
	dateTime = parser.parse(line_map["Authored On"])
	line_map["Authored On"] = str(dateTime)
	dateTime = parser.parse(line_map["Last Edited"])
	line_map["Last Edited"] = str(dateTime)
	map_list.append(line_map)


# sort list by Authored On date
sorted_map_list = sorted(map_list, key=itemgetter("Authored On"), reverse=True)

greenmap_name_for_url = "-" + sys.argv[1].lower()
print greenmap_name_for_url
if(greenmap_name_for_url == "-blekinge"):
	# blank for Blekinge greenmap
	greenmap_name_for_url = ""

print "<table>"
# loop map file
site_count = 0
new_sites_list = []
for line_map in sorted_map_list:
	new_sites_list.append(line_map)

	# print each entry matching current Primary term
	site_count = site_count+1

#	dateTime = parser.parse(line_map["Authored On"])
#	print dateTime

	if site_count % 2:
		print "<tr class=\"d2\"><td>"
	else:
		print "<tr class=\"d1\"><td>"

	icons = getIcons(line_map["Primary Term"])

	print "<a name=\"" + line_map["Name of Site"] + "\"></a>"
	if(line_map["Web Address"] == ""):
		print line_map["Name of Site"] + icons
	else:
		print "<a href=\"" + line_map["Web Address"] + "\" target=_blank>" + line_map["Name of Site"] + "</a>" + icons
	print "<br/>"

	print line_map["Details"].strip()
	info_line = "<br/>"
	if (line_map["Street location"]):
		info_line += line_map["Street location"].strip() + ", "
	if (line_map["City"]):
		info_line += line_map["City"].strip() + ", "
	if (line_map["Telephone"]):
		info_line += line_map["Telephone"].strip()
	info_line += "<br/>"
	info_line += "<a href=\"http://www.opengreenmap.org/sv/greenmap/blekinge-greenmap?autoBubbleNID=" + line_map["Site ID"] + "\" target=_blank><font style=\"background-color: #89c438;color:#f8ffe4\">Mer information och karta</font></a></p>"
	print info_line

	print "</td></tr>"

	if site_count >= MAX_NEW_SITES:
		break
print "</table>"
print "<br/>"

# sort list by Last edited date
sorted_map_list = sorted(map_list, key=itemgetter("Last Edited"), reverse=True)

print "Senast ändrade platser:"
print "<br/>"

print "<ul>"
site_count = 0
# loop map file
for line_map in sorted_map_list:
	# print each entry matching current Primary term

	site_in_list = False
	for new_list_site in new_sites_list:
		if new_list_site["Site ID"] == line_map["Site ID"]:
			site_in_list = True
	if not site_in_list:
		site_count = site_count+1
		print "<li><a href=\"http://ekoblekinge.se/grona-platser" + greenmap_name_for_url + "#" + line_map["Name of Site"] + "\">" + line_map["Name of Site"] + "</a></li>"
##		dateTime = parser.parse(line_map["Authored On"])
##		print dateTime

	if site_count > MAX_LATEST_SITES:
		break
print "</ul>"

